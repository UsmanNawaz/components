---
title: ProjectCard
description: A UI card showcasing a project. 
position: 1
category: Guide
---

This UI card component showcases a project. It has a large illustration and a call to action button to accompany the title and description.

## Design 

<cta-button link="https://www.figma.com/file/2gSU3IRPFTSquJg8RrRhMK/?node-id=1322%3A0" text="Figma Page"></cta-button>

## Vue + CSS

<project-card-css></project-card-css>

## Vue + Tailwind

_Coming soon_